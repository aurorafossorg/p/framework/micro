#ifndef uaurorafw_sensors_dht11_h
#define uaurorafw_sensors_dht11_h

#define DHT11_OK 0
#define DHT11_ERROR_CHECKSUM -1
#define DHT11_ERROR_TIMEOUT -2

int dht11_read(int pin, int* humidity, int* temperature);

#endif /* uaurorafw_sensors_dht11_h */